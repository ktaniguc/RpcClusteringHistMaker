#include "../RpcClusteringHistMaker/RpcClusterHistMaker.h"
#include "/home/ktaniguc/RootUtils/RootUtils/TLegend_addfunc.h"
#include "/home/ktaniguc/RootUtils/RootUtils/TCanvas_opt.h"

void RpcClusterHistMaker::drawHist(TString pdf)
{
  //==================================================================
  //Set Canvas
  //==================================================================
  //TCanvas *c1 = new TCanvas("c1", "c1", 10, 10, 1080, 700);
  //
  TCanvas_opt *c1 = new TCanvas_opt();
  gStyle->SetOptStat(0);
  // c1->SetGrid();
  // c1->SetRightMargin(0.20);
  // c1->SetLeftMargin(0.23);
  c1->SetTopMargin(0.20);
  c1->Print(pdf + "[", "pdf");

  h_tpextdR->Draw();
  c1->Print(pdf, "pdf");
  hh_probe_etaphi->Draw("colZ");
  c1->Print(pdf, "pdf");
  hh_probe_extetaphi->Draw("colZ");
  c1->Print(pdf, "pdf");
  h_tpmass->Draw();
  c1->Print(pdf, "pdf");
  h_probept->Draw();
  c1->Print(pdf, "pdf");
  
  h_n_clusterRoadMid_plausible->SetLineColor(kRed);
  TLegend_addfunc *lopt_3clus;
  lopt_3clus = new TLegend_addfunc(1, 2);
  //lopt_3clus->AddOption(h_n_clusterRoadMid_3clus, "N_{cluster} > 2");
  //lopt_3clus->AddOption(h_n_clusterRoadMid_plausible, "N_{cluster} > 1");
  h_n_clusterRoadMid_plausible->Draw();
  //h_n_clusterRoadMid_3clus->Draw("same");
  //lopt_3clus->Draw("same");
  c1->Print(pdf, "pdf");
  m_h_dSlope_offVSclusRoadMid->SetLineColor(kRed);
  m_h_dSlope_offVSroadMid->SetLineColor(kBlue);
  m_h_dSlope_offVSclusRoadMid_ineff->SetLineColor(kGreen);
  m_h_dSlope_offVSroadMid->Draw();
  m_h_dSlope_offVSclusRoadMid_ineff->Draw("same");
  m_h_dSlope_offVSclusRoadMid->Draw("same");
  TLegend_addfunc *lopt_dSlope;
  lopt_dSlope = new TLegend_addfunc(1, 3);
  lopt_dSlope->AddOption(m_h_dSlope_offVSclusRoadMid, "clusterRoad (N_{clusRoad}=1)");
  lopt_dSlope->AddOption(m_h_dSlope_offVSroadMid, "RPChit road (N_{clusRoad} = 1)");
  lopt_dSlope->AddOption(m_h_dSlope_offVSclusRoadMid_ineff, "clusterRoad (N_{clusRoad}>1)");
  lopt_dSlope->Draw("same");
  c1->Print(pdf, "pdf");
  double eff_x, eff_y, eff_xerr, eff_yerr;
  double eff_x_total = 0;
  int nbins = h_n_clusterRoadMid_plausible->GetXaxis()->GetNbins();
  cout << "DrawEfficiency::the efficiency parameter" << endl;
  cout << "eff_y/eff_yerr" << "   " << "numerator/denominator" << endl;
  for(unsigned int ibin = 1; ibin <= nbins; ibin++){
    double denominator = h_n_clusterRoadMid_plausible->GetBinContent(ibin);
    eff_x = h_n_clusterRoadMid_plausible->GetBinContent(ibin);
    eff_x_total = eff_x_total + eff_x;
    cout << "bin = " << ibin << ": " << eff_x << endl;
  }//ibin loop end
  cout << "eff_x total : " << eff_x_total << endl;
  c1->SetLogy(0);
  
  TLegend_addfunc *lopt_nroad;
  lopt_nroad = new TLegend_addfunc(1, 2);
  lopt_nroad->AddOption(h_n_clusterRoadMid_total, "total");
  lopt_nroad->AddOption(h_n_clusterRoadMid_plausible, "w/ similarRoad cut");
  h_n_clusterRoadOut_plausible->SetLineColor(kRed);
  h_n_clusterRoadOut_plausible->Draw();
  h_n_clusterRoadMid_3clus->Draw("same");
  lopt_3clus->Draw("same");
  c1->Print(pdf, "pdf");
  h_min_diffSlopeOffClusRoad->Draw();
  c1->Print(pdf, "pdf");
  h_min_diffSlopeOffRoad->Draw();
  c1->Print(pdf, "pdf");
  
  c1->SetLogy(); 
  m_hh_ptVSclusRoadMid->Draw("colZ");
  c1->Print(pdf, "pdf");
  m_hh_ptVSroadMid->Draw("colZ");
  c1->Print(pdf, "pdf");
  m_hh_ptVSbothRoadMid->Draw("colZ");
  c1->Print(pdf, "pdf");
  m_h_probept_1clusRoad->Draw();
  m_h_probept_1clusRoad_nice->SetLineColor(kRed);
  m_h_probept_1clusRoad_nice->Draw("same");
  TLegend_addfunc *lopt_bothroad;
  lopt_bothroad = new TLegend_addfunc(1, 3);
  lopt_bothroad->AddOption(m_h_probept_1clusRoad, "N_{clusRoad} = 1");
  lopt_bothroad->AddOption(m_h_probept_1clusRoad_nice, "|#theta_{clusRoad}-#theta_{off}|/|#theta_{road}-#theta_{off}| < 1");
  lopt_bothroad->Draw("same");
  c1->Print(pdf, "pdf");
  if(TEfficiency::CheckConsistency(*m_h_probept_1clusRoad_nice, *m_h_probept_1clusRoad)){
    m_eff_roadpfm = new TEfficiency(*m_h_probept_1clusRoad_nice, *m_h_probept_1clusRoad);
    m_eff_roadpfm->SetTitle("Ratio of |#theta_{clusRoad}-#theta_{off}|/|#theta_{road}-#theta_{off}| < 1 events;probe p_{T}[GeV/c];ratio");
  }
  m_eff_roadpfm->Draw();
  c1->Print(pdf, "pdf");

  //profiling
  prof_ptVSclusRoadMid = m_hh_ptVSclusRoadMid->ProfileX();
  prof_ptVSroadMid = m_hh_ptVSroadMid->ProfileX();
  prof_ptVSclusRoadMid->SetErrorOption("S");
  prof_ptVSclusRoadMid->SetLineColor(kRed);
  prof_ptVSroadMid->SetErrorOption("S");
  prof_ptVSroadMid->Draw();
  prof_ptVSclusRoadMid->Draw("same");
  TLegend_addfunc *lopt_prof;
  lopt_prof = new TLegend_addfunc(1, 3);
  lopt_prof->AddOption(prof_ptVSclusRoadMid, "clusterRoad");
  lopt_prof->AddOption(prof_ptVSroadMid, "RPC hit Road");
  lopt_prof->Draw("same");
  c1->Print(pdf, "pdf");

  c1 -> Print( pdf + "]", "pdf" );
  delete c1;
}

