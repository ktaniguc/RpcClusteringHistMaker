#define RpcClusterHistMaker_cxx
#include "../RpcClusteringHistMaker/RpcClusterHistMaker.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <vector>
#include "/home/ktaniguc/RootUtils/src/rootlogon.C"

int main(int argc, char *argv[])
{
  rootlogon();
//  TColor::InvertPalette();
  TChain *tree1 = new TChain("t_tap", "t_tap");
  
  TString PdfLabel = argv[1];
  tree1 -> Add(argv[2]);
  Int_t limit_entry = atoi(argv[3]);
  Int_t tap_type = atoi(argv[4]);
  Int_t trig_chain = atoi(argv[5]);
 // cout << "limit entry = "<< limit_entry << endl;
  
  TFile *fout = new TFile(Form("~/RpcClusteringHistMaker/src/outroot/%s.root", PdfLabel.Data()), "recreate");

  RpcClusterHistMaker t_clus(tree1);
  t_clus.Loop(limit_entry, trig_chain);
  t_clus.drawHist("~/RpcClusteringHistMaker/plot/DrawHist_" + PdfLabel + ".pdf");
  fout -> Write();

  return 0;
}

void RpcClusterHistMaker::Loop( int Nevents, int trig_chain)
{

   if (fChain == 0) return;

   int nLoop = (Nevents == -1) ? fChain->GetEntries() : Nevents;

   //Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nLoop;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
      
      /*global event cut*/
      if(!(abs(probe_exteta) < 1.05)) continue;
      cout << "sumReqdRL1/tp_extdR = " << sumReqdRL1 << "/" << tp_extdR << endl;
      cout << "sumReqdREF/tp_dR = " << sumReqdREF << "/" << tp_dR << endl;
      if(!(probe_mesEFTAG_pass -> at(trig_chain) > -1 && ( sumReqdRL1<tp_extdR /*0.1<tp_extdR*/ ) && ( sumReqdREF<tp_dR ))) continue;
      if(!(probe_mesSA_pass->at(trig_chain) == 1)) continue;
      if(!(probe_mesSA_isRpcFailure->at(trig_chain) == 0)) continue;
      /* global cut end*/

      fillHist( trig_chain );
   }
}

