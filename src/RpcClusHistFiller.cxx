#include "../RpcClusteringHistMaker/RpcClusterHistMaker.h"
#include "TVector3.h"

void RpcClusterHistMaker::fillHist( int trig_chain)
{
  //RpcL2MuonSA/src/RPC.cxx L3553~ will be 
  //helpful to know what cut is required

    unsigned int nClus = probe_mesSA_rpcClusX->at(trig_chain).size();
    unsigned int nHit = probe_mesSA_rpcHitX->at(trig_chain).size();
    unsigned int nClus_fit = probe_mesSA_rpcClusIsPlausibleFitInnMid->at(trig_chain).size();
    vector<float> OffsegMidSlope, OffsegOutSlope;
    OffsegMidSlope.clear();
    OffsegOutSlope.clear();
    
    for(int iSeg = 0; iSeg < probe_segment_n; iSeg++){
      TVector3 vecProbeOffseg;
      TVector3 vecProbeOffsegDir;
      vecProbeOffseg.SetXYZ(probe_segment_x[iSeg], probe_segment_y[iSeg], probe_segment_z[iSeg]);
      vecProbeOffsegDir.SetXYZ(probe_segment_px[iSeg], probe_segment_py[iSeg], probe_segment_pz[iSeg]);
      cout << "Probe segment:: " << vecProbeOffseg.Perp() << endl;
      if(6500. < vecProbeOffseg.Perp() && vecProbeOffseg.Perp() < 8500.){
        OffsegMidSlope.push_back(vecProbeOffsegDir.Perp()/probe_segment_pz[iSeg]);
      }
      if(9500. < vecProbeOffseg.Perp() && vecProbeOffseg.Perp() < 10500.){
        OffsegOutSlope.push_back(vecProbeOffsegDir.Perp()/probe_segment_z[iSeg]);
      }
    }

    hh_probe_etaphi->Fill(probe_eta, probe_phi);
    hh_probe_extetaphi->Fill(probe_exteta, probe_extphi);
    h_tpextdR->Fill(tp_extdR);
    h_tpmass->Fill(tp_mass/1000);
    h_probept->Fill(probe_pt/1000);
  
    cout << "probe_pt = " << probe_pt << ", " << "mes_n = " << mes_n << endl;
    cout << "probe_mesSA_pt = " << probe_mesSA_pt->at(trig_chain) << ", probe_mesSA_pass = " << probe_mesSA_pass->at(trig_chain) << ", probe_mesSA_isRpcFailure = " << probe_mesSA_isRpcFailure->at(trig_chain) << endl;
    cout << "nClus/nHit = " << nClus << "/" << nHit << endl;
    cout << "road: " << probe_mesSA_roadAw -> at(trig_chain)[0] << ": " << probe_mesSA_roadBw->at(trig_chain)[0] << endl;
    cout << "road: " << probe_mesSA_roadAw -> at(trig_chain)[1] << ": " << probe_mesSA_roadBw->at(trig_chain)[1] << endl;
    cout << "road: " << probe_mesSA_roadAw -> at(trig_chain)[2] << ": " << probe_mesSA_roadBw->at(trig_chain)[2] << endl;
  
    h_n_clusterRoadMid_total->Fill(nClus_fit);

    int countPlausibleFitInnMid = 0;
    int countPlausibleFitOut = 0;
    int count3clusInnMid = 0;
    int count3clusOut = 0;
    vector<float> PlausibleFitMidSlope, PlausibleFitOutSlope;
    PlausibleFitMidSlope.clear();
    PlausibleFitOutSlope.clear();
    for(unsigned int iClus_fit = 0; iClus_fit < nClus_fit; iClus_fit++){
      int Nclusinset = 0;
      for(unsigned int ilay = 0; ilay < 8; ilay++){
        if(probe_mesSA_rpcClusIdInSets->at(trig_chain).at(ilay).at(iClus_fit) > -1){
          Nclusinset++;
        }
      }
      if(probe_mesSA_rpcClusIsPlausibleFitInnMid->at(trig_chain).at(iClus_fit)){ 
        if(Nclusinset > 2){
          count3clusInnMid++;
        }
        countPlausibleFitInnMid++;
        PlausibleFitMidSlope.push_back(probe_mesSA_rpcClusFitMidSlope->at(trig_chain).at(iClus_fit));
      }
      if(probe_mesSA_rpcClusIsPlausibleFitOut->at(trig_chain).at(iClus_fit)){ 
        if(Nclusinset > 2){
          count3clusOut++;
        }
        countPlausibleFitOut++;
        PlausibleFitOutSlope.push_back(probe_mesSA_rpcClusFitOutSlope->at(trig_chain).at(iClus_fit));
      }
    }//iClus_fit loop
    if(countPlausibleFitInnMid == 0){
      cout << "======= this event has no clusterRoad ======" << endl;
      cout << "nClus/nHit = " << nClus << "/" << nHit << endl;
      cout << "rpc hit road info" << endl;
      cout << " road(inner) : " << probe_mesSA_roadAw -> at(trig_chain)[0] << ": " << probe_mesSA_roadBw->at(trig_chain)[0] << endl;
      cout << " road(middle): " << probe_mesSA_roadAw -> at(trig_chain)[1] << ": " << probe_mesSA_roadBw->at(trig_chain)[1] << endl;
      cout << " road(outer) : " << probe_mesSA_roadAw -> at(trig_chain)[2] << ": " << probe_mesSA_roadBw->at(trig_chain)[2] << endl;
      cout << "offline eta/phi (at MS) = " << probe_exteta << "/" << probe_extphi << endl;
      cout << "============================================" << endl;
    }
    h_n_clusterRoadMid_plausible->Fill(countPlausibleFitInnMid);
    h_n_clusterRoadMid_3clus->Fill(count3clusInnMid);
    h_n_clusterRoadOut_3clus->Fill(count3clusOut);
    h_n_clusterRoadOut_plausible->Fill(countPlausibleFitOut);

    //////if the number of clusterRoadMid = 1, then check the performance if clusterRoad
    if(countPlausibleFitInnMid == 1 && OffsegMidSlope.size() == 1){
      double RoadMid = probe_mesSA_roadAw->at(trig_chain)[1];
      /*cout << "the number of clusterRoad(middle) is 1 :" << endl;
      cout << "Offline R/Z              = " <<  OffsegMidSlope.at(0) << endl;
      cout << "clusterRoad(middle) R/Z  = " << PlausibleFitMidSlope.at(0) << endl;
      cout << "RPC hit road(middle) R/Z = " << RoadMid << endl;*/

      double atan_clusRoadMid = atan(1/PlausibleFitMidSlope.at(0));
      double atan_offsegMid = atan(1/OffsegMidSlope.at(0));
      double atan_roadMid = atan(1/RoadMid);
      double ratio = abs(atan_clusRoadMid-atan_offsegMid)/abs(atan_roadMid-atan_offsegMid);
      m_h_dSlope_offVSclusRoadMid->Fill(atan_clusRoadMid - atan_offsegMid);
      m_h_dSlope_offVSroadMid->Fill(atan_roadMid - atan_offsegMid);
      m_hh_ptVSclusRoadMid->Fill(probe_pt/1000, atan_clusRoadMid-atan_offsegMid);
      m_hh_ptVSroadMid->Fill(probe_pt/1000, atan_roadMid-atan_offsegMid);
      m_hh_ptVSbothRoadMid->Fill(probe_pt/1000, abs(atan_clusRoadMid-atan_offsegMid)/abs(atan_roadMid-atan_offsegMid));
      m_h_probept_1clusRoad->Fill(probe_pt/1000);
      if(ratio < 1) m_h_probept_1clusRoad_nice->Fill(probe_pt/1000);
    }
    else if(countPlausibleFitInnMid > 1 && OffsegMidSlope.size() == 1){
      for(unsigned int i_clusRoad = 0; i_clusRoad < countPlausibleFitInnMid; i_clusRoad++){
        m_h_dSlope_offVSclusRoadMid_ineff->Fill(atan(1/PlausibleFitMidSlope.at(i_clusRoad)) - atan(1/OffsegMidSlope.at(0)));
      }
    }
    //////

    float min_diffSlopeOffClusRoad = 9999;
    float min_diffSlopeOffRoad = 9999;
    for(unsigned int iSegMid = 0; iSegMid < OffsegMidSlope.size(); iSegMid++){
      for(unsigned int iClus_set = 0; iClus_set < PlausibleFitMidSlope.size(); iClus_set++){
        float diffSlope = fabs(OffsegMidSlope.at(iSegMid) - PlausibleFitMidSlope.at(iClus_set));
        if(min_diffSlopeOffClusRoad > diffSlope) min_diffSlopeOffClusRoad = diffSlope;
      }//iClus_set loop
      cout << "min_diffSlopeMid = " << min_diffSlopeOffClusRoad << endl;
      h_min_diffSlopeOffClusRoad->Fill(min_diffSlopeOffClusRoad);
      
      float diffSlope_l2saroad = fabs(OffsegMidSlope.at(iSegMid) - probe_mesSA_roadAw->at(trig_chain)[1]);
      if(min_diffSlopeOffRoad > diffSlope_l2saroad) min_diffSlopeOffRoad = diffSlope_l2saroad;

    }//iSegMid  
    h_min_diffSlopeOffRoad->Fill(min_diffSlopeOffRoad);
   
}
