#!/bin/sh
PDF_LABEL="rpcCluster_Ztap"
#INPUT_NTUPLE="/home/ktaniguc/outputfile/mc16_13TeV.Jpsi_mu3p5mu3p5.NoTag.FCBM.AddLayer.root"
#INPUT_NTUPLE="/gpfs/fs7001/ktaniguc/outputfile/OutputCalcEff/RpcCluster/RpcCluster_201912081500_JpsitapMC.root"
#INPUT_NTUPLE="/gpfs/fs7001/ktaniguc/outputfile/test_OutputCalcEff/RpcCluster/RpcCluster_201911131630.root"
#INPUT_NTUPLE="/gpfs/fs7001/ktaniguc/outputfile/OutputCalcEff/RpcCluster/RpcCluster_20191228_addMoreCand-MinBias_Ztap.root"
#INPUT_NTUPLE="/gpfs/fs7001/ktaniguc/outputfile/OutputCalcEff/RpcCluster/RpcCluster_20200128_ZmumuMinbias-Allow2clusSet.Ztap.root"
#INPUT_NTUPLE="/gpfs/fs7001/ktaniguc/outputfile/OutputCalcEff/RpcCluster/RpcCluster_20200217_Zmumu-2LayerSearch_350files_Ztap.root"
#INPUT_NTUPLE="/gpfs/fs7001/ktaniguc/outputfile/OutputCalcEff/RpcCluster/RpcCluster_20200117_addMoreCand-MinBias_Ztap.root"
#INPUT_NTUPLE="/gpfs/fs7001/ktaniguc/outputfile/OutputCalcEff/RpcCluster/RpcCluster_20200120_Jpsimu4mu20-JpsitapMC.root"
#INPUT_NTUPLE="/home/ktaniguc/outputCalcEff/RpcCluster_clusterRoadInfo10281600.root"
#INPUT_NTUPLE="/gpfs/fs7001/ktaniguc/outputfile/OutputCalcEff/RpcCluster/RpcCluster_20200301_Zmumu-recoveredSingleClusRoad_partial_Ztap.root"
INPUT_NTUPLE="/gpfs/fs7001/ktaniguc/outputfile/OutputCalcEff/RpcCluster/RpcCluster_20200317_Zmumu_usingPhiInfo_Ztap.root"

#LIMIT_ENTRY=200
LIMIT_ENTRY=-1
TAP_TYPE=3
TRIG_CHAIN=0

echo ""
echo "PDF_LABEL: "${PDF_LABEL}
echo "INPUT_NTUPLE: "${INPUT_NTUPLE}
echo ""

COMMAND="./HistMaker.exe ${PDF_LABEL} ${INPUT_NTUPLE} ${LIMIT_ENTRY} ${TAP_TYPE} ${TRIG_CHAIN}"
LOG="log/log_"${PDF_LABEL}

eval ${COMMAND} 2>&1 | tee ${LOG}

#eof

